<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Nuban extends Model
{
    use HasFactory;

    //protected $table = 'nubans';

    protected $fillable = ['account', 'name'];

}
