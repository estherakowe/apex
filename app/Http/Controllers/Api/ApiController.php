<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Nuban;


class ApiController extends Controller
{
  public function index()
{
$nuban = Nuban::all();
return response()->json([
"success" => true,
"message" => "Nuban List",
"data" => $nuban
]);
}


public function show( $id)
{
// check if Nuban exist and return corresponding account details
$nuban = Nuban::where('id', $id)->first();

if ($nuban) {
return response()->json([
"success" => true,
"message" => "Account retrieved successfully.",
"data" => Nuban::find($id)
]);
} else {
return response()->json([
"success" => false,
"message" => "Account not found."
]);
    }
}


public function store(Request $request)
{
$input = $request->all();
// accept same reference used for validation
$validator = Validator::make($input, [
'name' => 'required|min:3',
'account' => 'required|min:10' // do not process duplicate account number
]);
if($validator->fails()){
return $this->sendError('Validation Error.', $validator->errors());       
}
$nuban = Nuban::create($input);
return response()->json([
"success" => true,
"message" => "account created successfully.",
"data" => $nuban
]);
} 

public function update(Request $request, Nuban $nuban)
{
$input = $request->all();
$validator = Validator::make($input, [
'name' => 'required',
'account' => 'required'
]);
if($validator->fails()){
return $this->sendError('Validation Error.', $validator->errors());       
}
$nuban->name = $input['name'];
$nuban->account = $input['account'];
$nuban->save();
return response()->json([
"success" => true,
"message" => "Account updated successfully.",
"data" => $nuban
]);
}

public function delete(Nuban $nuban)
{
$nuban->delete();
return response()->json([
"success" => true,
"message" => "Account deleted successfully.",
"data" => $nuban
]);
}   
}

